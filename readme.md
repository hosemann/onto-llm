# Disclaimer
## Documentation
The program was not written with publication of the code in mind and it is neither documented nor actively maintained. It has not been tested on Windows.
However if you encounter any problems while running the program feel free to reach out to **simon.hosemann@uni-leipzig.de**.
## Cost and first use
**IMPORTANT**: Even though this program itself is free, using it will generate costs since the openAI API is not free of charge. Consult [openAI pricing](https://openai.com/pricing) to see the current pricing policy. Often openAI provides free credit for new API users. If you are a new API user or don't have a paid account, keep in mind that for new and non-paying users the API rate limits are quite restrictive, which will result in performance issues when using this program. Currently (September '23) even for paid users the rate limits are restricted during the first 48 hours after making the first payment. Consult the [openAI policy on rate limits](https://platform.openai.com/docs/guides/rate-limits/overview) for further information.

# What this program does
This program creates an IS-A hierarchy by repeatedly prompting a large language model. Starting from a seed concept, it iteratively explores subconcepts up until a specified depth. 
The language model used is gpt-3.5-turbo. It is accessed through the chat completions endpoint of the openAI API (version 1). 

# Reference
For background and a high-level description of the implemented algorithm consider the publication [Towards Ontology Construction with Language Models](https://www.informatik.uni-leipzig.de/kr/research/papers/2023/onto_llm.pdf). There is also an [overview of some hierarchies](https://www.informatik.uni-leipzig.de/kr/onto-llm/) created with the program.

# Prerequisites
- A key for the openAI API.
- Optional: an organization ID for the openAI API.
- Installation of Python, version 3.11 or newer.
- The default package manager for Python: pip.

# Set-up
1. Clone the repository.
2. Run `python3 -m venv llm-venv` to create virtual environment named llm-venv.
3. Enter the new virtual environment by running `source llm-venv/bin/activate` (Mac/Linux) or `llm-venv\Scripts\activate` (Windows).
4. Install dependencies: `pip install -r requirements.txt`.

# Running the program
1. If not already in use, enter the virtual environment by running `source llm-venv/bin/activate` (Mac/Linux) or `llm-venv\Scripts\activate` (Windows).
2. Run the program: `python onto-llm.py`.
3. The first time you run the program, you will be prompted for your openAI API credentials (see the next two points, 4. API key and 5. Organization ID).
4. API key. It will be stored in `credentials/openai_key.txt`.
5. Organization ID. This is optional. If you provide one, it will be stored in `credentials/openai_organization_id.txt`.
6. You are asked if you want to change your preferences. These include credentials, caching and threshold preferences and the outdegree parameter. Preferences are stored in `/config.json`.
7. You are prompted for a seed concept (e.g., Animal). This is the concept for which subconcepts will be explored.
8. The program asks for a depth limit for subconcept exploration (e.g., 2).
9. You will be prompted for a frequency threshold; common values range from 5 to 20.
10. If activated, you are prompted for the outdegree parameter.

# Output
- You can find the output in the ontologies directory, for every created ontology there is a separate subdirectory.
- The output is provided in the formats `.svg`, `.owl` and `.json`.
- Some metadata is found in the file ending on `metadata.json`. If caching is used the cost in the metadata file is calculated as if there was no caching. The actual cost will be lower.
- If the `.owl` output doesn't work as expected or is corrupted this is likely due to unescaped special symbols.
- In the `.svg` output grey arrows indicate that concepts are not further explored due to a limit of the outdegree set by the user. Blue arrows mark synonymous concept.

# Short description of parameters, caching and thresholds
- Exploration depth: Up to this depth new concepts will be explored, the depth of a concept is its shortest path to the seed concept.
- Frequency threshold: This is a tradeoff between completeness (lower values like 5) and correctness (higher values like 20), different domains demand different values for reasonable output.
- Outdegree (initially deactivated): Limits the number of subconcept considered for any concept.
- Prompt caching (initially activated): caching of all prompts to the language model and pairs of concepts and concept descriptions.
- Sample caching (initially deactivated): before exploring subconcepts we take a sample of the first token of the answer of size 100 to determine for which completions to prompt. This sample can be cached separately.
- Cost threshold in dollars (initially set to 30): if the cost threshold is exceeded, the program will terminate in an orderly fashion.
- Concept threshold (initially set to 3000): if the concept threshold is exceeded, the program will terminate in an orderly fashion.
- Time threshold in minutes (initially set at 300): if the time threshold is exceeded, the program will terminate in an orderly fashion.

# Debugging and common problems
- If you manually changed the config file and want to reset it, just delete the config file and run the program again.
- If you are prompted to enter a description manually for multiple seed concepts, this indicates that there is something wrong with your openAI credentials. Delete the credentials directory and restart the program. Re-enter your credentials. If the problem persists check your settings at the openAI API, it is likely a billing issue.
- The rate limits of the openAI API are rather restrictive for non-paying users and even for paying users and organizations during the first 48 hours. If you experience slow running times or multiple timeouts, this is most likely due to the API access, which is the program's bottleneck in terms of speed.

# Feedback
If you want to give any kind of feedback, feel free to reach out via email  **simon.hosemann@uni-leipzig.de**.